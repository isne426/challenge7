#ifndef STRVAR_H
#define STRVAR_H
#include <iostream>
using namespace std;
namespace strvarken
{
	class StringVar   																					//the header
	{
		public:
			StringVar(int size);
			StringVar();
			StringVar(const char a[]);
			StringVar(const StringVar& string_object);
			~StringVar();																				//declairing the string fn
		
			int length() const;
			void input_line(istream& ins);																//declairing variables
		
			friend ostream& operator <<(ostream& outs, const StringVar& the_string);
			friend istream& operator >> (istream& ins, const StringVar& the_string);
			friend string operator+ (StringVar s1, StringVar s2);
			friend bool operator== (StringVar s1, StringVar s2);
		
			string toString();
			int maxLength();
			string copy_piece(int i, int j);
			char one_char(int index);
			void set_char(int index, char c);

	private:
		char *value; 
		int max_length; 
	};
}																										//all above strvarken
#endif 																									//STRVAR_H

